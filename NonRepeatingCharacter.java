//{ Driver Code Starts
//Initial Template for Java

import java.io.*;
import java.lang.*;
import java.util.*;

class Driverclass
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        
        while(t-- > 0)
        {
            String st = sc.next();
            
            char ans=new Solution().nonrepeatingCharacter(st);
            
            if(ans!='$')
            System.out.println(ans);
            else
            System.out.println(-1);
        }
    }
}

// } Driver Code Ends


class Solution
{
    // Function to find the first non-repeating character in a string.
    static char nonrepeatingCharacter(String S)
    {
        // Map to store the count of each character
        Map<Character, Integer> countMap = new LinkedHashMap<>();
        
        // Iterate through the string to count occurrences of each character
        for (char c : S.toCharArray()) {
            countMap.put(c, countMap.getOrDefault(c, 0) + 1);
        }
        
        // Iterate through the map to find the first non-repeating character
        for (Map.Entry<Character, Integer> entry : countMap.entrySet()) {
            if (entry.getValue() == 1) {
                return entry.getKey();
            }
        }
        
        // If no non-repeating character found, return '$'
        return '$';
    }
}
